Modalys is IRCAM’s flagship physical model-based sound synthesis environment, used to create virtual instruments from elementary physical objects such as strings, plates, tubes, membranes, plectra, bows, or hammers.

It is also possible to create objects with more complex shapes out of 3D meshes, or using measurements, and Modalys does all the hard computational work for you, bringing them to life and making them sound.

By combining these various physical objects, astonishing, unique virtual instruments can be designed, and it is up to you to decide how to play them!

[Max/MSP](https://forum.ircam.fr/projects/detail/max-8/) (Modalys for Max) or [OpenMusic](https://forum.ircam.fr/projects/detail/openmusic/) (Modalys Library) both offer some friendly environment to deal with the synthesis engine of Modalys, via an intuitive graphical interface.

## Main Applications ##

- **Music & Composition**: Avant-garde/techno, combined with acoustic instruments
- **Sound Design**: natural work on the quality of sound (metallic, wood, etc.)
- **Education**: “modal theory” in practice
- **Cinema & Video**: virtual and hybrid sound design
- **Scientific Research & Development**: comparison of theoretically determined and measured modes

## Main Functions ##

At the heart of Modalys is an engine used to create virtual instruments using simple physical objects, measurements, or even 3D meshes. Create instruments and then decide how they should be played & controlled. Here is the typical design of a virtual instrument:

- Select basic objects that will be used to make the instrument (e.g. tube, string, membrane, plate, plectra, bow, hammer, etc.). These objects have standard settings that can be entirely customized if desired.
- Specify accesses to the objects, i.e. locations on the objects where interactions happen.
- Add connections corresponding to playing styles: strike, pluck, blow, bow, etc.
- Add controllers to change playing style parameters in real time.

## Additional Information for Product ##

**Modalys for Max**, chosen by many composers or sound designers, is Modalys embodied in [Max/MSP](https://forum.ircam.fr/projects/detail/max-8/) real-time environment, and is used to graphically build sophisticated instruments from strings, plates, membranes, and tubes of virtually any kind of material (metal, wood, rubber, glass, etc.), size, and interactions: striking, pinching, bowing, blowing etc. Modalys for Max notably includes a new scriptural design approach based on (just-in-time) Lua language: **mlys.lua**. Modalys for Max features many examples and help files, and is compatible with Max/MSP 6 or higher (including 8.x) on Mac OS X (10.10 or later, including Big Sur) and Windows (7 or later).

A special version of **Medit** (a 3D viewer originally designed by INRIA) taylored for Modalys is included in the package.

Also, Modalys can be invoked from **ModaLisp**, a textual programming environment based on Lisp, and can be used to define physical models in a more programming kind of way, similarly to **mlys.lua** but in a non-realtime environment. IRCAM’s [OpenMusic](https://forum.ircam.fr/projects/detail/openmusic/) music composition program, also using Lisp behind the hood, offers another interesting  graphic control over the models, for instance using curves or score fragments.

> - Developed by [Sound Systems and Signals: Audio/Acoustics, InstruMents](https://www.ircam.fr/recherche/equipes-recherche/systemes-et-signaux-sonores-audioacoustique-instruments-s3am/)
>
> **[Training Courses]( https://www.ircam.fr/transmission/formations-professionnelles/)**
>
>[Modalys](https://www.ircam.fr/agenda/modalys-2022/detail/)
/ 18 hours of training. Wednesday-Friday, January 19-21, 2022. 10am-1pm/2:30pm-5:30pm 
>
> Tutorials by [Viking Foolez](https://www.youtube.com/user/maestrorulez)
>
> - [The Plucked String Radiation - Modalisp/OpenMusic/Max](https://www.youtube.com/watch?v=__Xda1W5ZwY)
>
> - [Bowing Spiderman - Modalisp/OpenMusic/Max](https://www.youtube.com/watch?v=hZtH4uY09A0)
>
> - [The Confused Flatulence Tube - Modalisp/OpenMusic/Max](https://www.youtube.com/watch?v=VVX2FU1OxZQ)
>
> - [The Reed Of Distress - Modalisp/OpenMusic/Max](https://www.youtube.com/watch?v=D8wQu7F-l1U)
>
> - [Forcing The Membrane - Modalisp/OpenMusic/Max](https://www.youtube.com/watch?v=IC7C74NFYFs)
>
> - [I Felt The Sapphire - Modalisp/OpenMusic/Max](https://www.youtube.com/watch?v=KRzX6NHY5Ys)
